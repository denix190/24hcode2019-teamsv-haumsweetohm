#!/usr/bin/python3
import time
import sys
from laumio import *

# publish.single("laumio/Laumio_104F03/json", "{  'command': 'animate_rainbow' }", hostname="mpd.lan", port=1883)


laumio1 = Laumio("Laumio_1D9486")
laumio3 = Laumio("Laumio_0FBFBF")
laumio4 = Laumio("Laumio_104F03")
laumio6 = Laumio("Laumio_10805F")
laumio7 = Laumio("Laumio_CD0522")
laumio8 = Laumio("Laumio_0FC168")
laumio9 = Laumio("Laumio_D454DB")
laumio10 = Laumio("Laumio_107DA8")
laumio11 = Laumio("Laumio_88813D")



# Tout éteindre

#laumio/all/fill 0 0 0

laumio11.wipeOut()



#Allumage par colonne
#[1, 3, 4]
#sleep(2)
#laumio/1D9486/set_column 1 255 0 255
#laumio/0FBFBF/set_column 1 255 0 255
#laumio/104F03/set_column 1 255 0 255
laumio11.fillColumn(0, 255, 0, 255)
sleep(2)
#laumio/1D9486/set_column 1 0 255 255
#laumio/0FBFBF/set_column 1 0 255 255
#laumio/104F03/set_column 1 0 255 255
laumio11.fillColumn(1, 0, 255, 255)
sleep(2)
#laumio/1D9486/set_column 1 255 255 0
#laumio/0FBFBF/set_column 1 255 255 0
#laumio/104F03/set_column 1 255 255 0
laumio11.fillColumn(2, 255, 255, 0)
sleep(2)
#laumio/1D9486/set_column 1 255 0 0
#laumio/0FBFBF/set_column 1 255 0 0
#laumio/104F03/set_column 1 255 0 0
laumio11.fillColumn(3, 255, 0, 0)

 



