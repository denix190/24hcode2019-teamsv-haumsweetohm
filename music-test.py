#!/usr/bin/python3
import time
import paho.mqtt.client as mqtt
import sys
from time import sleep
import paho.mqtt.publish as publish
from laumio import *

# publish.single("laumio/Laumio_104F03/json", "{  'command': 'animate_rainbow' }", hostname="mpd.lan", port=1883)


laumio1 = Laumio("Laumio_1D9486")
laumio3 = Laumio("Laumio_0FBFBF")
laumio4 = Laumio("Laumio_104F03")
laumio6 = Laumio("Laumio_10805F")
laumio7 = Laumio("Laumio_CD0522")
laumio8 = Laumio("Laumio_0FC168")
laumio9 = Laumio("Laumio_D454DB")
laumio10 = Laumio("Laumio_107DA8")
laumio11 = Laumio("Laumio_88813D")



publish.single("music/control/setvol", "90", hostname="mpd.lan", port=1883)
publish.single("music/control/play", "", hostname="mpd.lan", port=1883)
